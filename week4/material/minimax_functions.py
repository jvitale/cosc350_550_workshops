from tictactoe_game_environment import TicTacToeGameEnvironment as gm
from une_ai.models import GraphNode

def minimax(node, player, depth):
    return 0, None

def minimax_alpha_beta(node, player, alpha, beta, depth):
    return 0, None

def optimised_minimax(node, player, tt, depth):
    return 0, None