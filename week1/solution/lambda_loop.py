first_list = [8, 10, 9]
second_list = [5, 7, 1, 2]

lambdas = []
for first_item in first_list:
    for second_item in second_list:
        # we need to add two parameters to the lambda function
        # and we can pass the desired items as the default
        # values for this lambda function
        cur_lambda = lambda a=first_item, b=second_item: a + b
        lambdas.append(cur_lambda)

results = []
for f in lambdas:
    results.append(f())

print("Results: ", results)