import numpy as np

def multiply_item(original_matrix, r, c, multiplier):
    # we need to create a copy of the original matrix
    # so that we do not risk to modify it
    matrix_copy = original_matrix.copy()
    # now we can modify the item in the copy of the matrix
    matrix_copy[r, c] *= multiplier
    # and return it
    return matrix_copy

my_matrix = np.array([[1, 2, 3], [4, 5, 6]])
print("Your newly created matrix:", my_matrix)

result_matrix = multiply_item(my_matrix, 1, 1, 2)

print("Your matrix now:", my_matrix)
print("Resulting matrix:", result_matrix)