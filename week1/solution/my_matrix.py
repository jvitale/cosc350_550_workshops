import numpy as np

# creating a function to retrieve valid inputs from the user
def get_valid_input(prompt, validation_function, error_message):
    # setting return value to None
    value = None

    # looping until we do not have a valid value
    while value is None:
        # retrieving the input from the user
        value = input(prompt)

        # checking the value fits the validation criteria
        if not validation_function(value):
            # invalid input
            # we notify the user with the error message
            print(error_message)
            # set value back to None so we loop again
            value = None
    
    # returning the valid value
    return value

def main():
    my_matrix = np.zeros((3, 4), dtype=int)

    print("Please, input the values for the matrix")
    for r in range(0, my_matrix.shape[0]):
        print("Row {0}:".format(r))
        for c in range(0, my_matrix.shape[1]):
            value = get_valid_input(
                "Please, input item {0} for the current row:".format(c),
                lambda v: v.isnumeric(),
                "Invalid input. The values must be int."
            )
            
            # if we are here, it means the value is valid
            # we add it to the matrix
            my_matrix[r, c] = int(value)
    
    # asking for the operation to perform
    valid_operations = ['mean', 'sum', 'std']
    operation = get_valid_input(
        "Please, select the desired operation to perform (mean, sum or std):",
        lambda v: v in valid_operations,
        "The selected operation is invalid."
    )
    
    # asking for the row / column
    valid_choices = ['row', 'colum']
    choice = get_valid_input(
        "Do you want to compute the operation on a row or on a column?",
        lambda v: v in valid_choices,
        "Invalid input value. The input must be 'row' or 'column'"
    )

    # asking for the index
    valid_idx_row = range(0, my_matrix.shape[0])
    valid_idx_col = range(0, my_matrix.shape[1])
    idx = get_valid_input(
        "Specify the index of the {0}:".format(choice),
        lambda v: int(v) in valid_idx_row if choice == 'row' else int(v) in valid_idx_col,
        "Invalid input value. The input must be a valid positive index."
    )
    idx = int(idx)

    if choice == 'row':
        items = my_matrix[idx, :]
    else:
        items = my_matrix[:, idx]
    
    if operation == 'mean':
        result = np.mean(items)
    elif operation == 'sum':
        result = np.sum(items)
    else:
        result = np.std(items)
    
    print("Your matrix:")
    print(my_matrix)
    print("The operation '{0}' applied on the {1} at index {2} gives the result {3}".format(operation, choice, idx, result))

if __name__ == '__main__':
    main()