first_list = [8, 10, 9]
second_list = [5, 7, 1, 2]

lambdas = []
for first_item in first_list:
    for second_item in second_list:
        cur_lambda = lambda: first_item + second_item
        lambdas.append(cur_lambda)

results = []
for f in lambdas:
    results.append(f())

print("Results: ", results)