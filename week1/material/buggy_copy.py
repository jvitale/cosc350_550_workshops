import numpy as np

def multiply_item(original_matrix, r, c, multiplier):
    original_matrix[r, c] *= multiplier
    return original_matrix

my_matrix = np.array([[1, 2, 3], [4, 5, 6]])
print("Your newly created matrix:", my_matrix)

result_matrix = multiply_item(my_matrix, 1, 1, 2)

print("Your matrix now:", my_matrix)
print("Resulting matrix:", result_matrix)