import csv

class Sample():

    def __init__(self, sample_vector, features, decision_feature):
        self._x = {}
        self._y = {}
        for i, feature in enumerate(features):
            if feature != decision_feature:
                self._x[feature] = sample_vector[i]
            else:
                self._y[feature] = sample_vector[i]
    
    def get_sample_values(self):
        sample_vector = []
        for value in self._x.values():
            sample_vector.append(value)
        return sample_vector
    
    def get_feature_value(self, feature):
        assert feature in self._x.keys() or feature in self._y.keys(), "'{0}' is not a valid feature.".format(feature)

        if feature in self._x.keys():
            return self._x[feature]
        else:
            return self.get_label()
    
    def get_label(self):
        decision_feature = list(self._y.keys())[0]
        return self._y[decision_feature]
    
    def __str__(self):
        return str(self._x) + ' -> ' + str(self._y)

class SamplesSet():

    def __init__(self, guess_who_file='guess_who.csv', decision_feature='Name'):
        self._features_values = {}
        self._features = []
        self._decision_feature = decision_feature
        self._samples = []

        with open(guess_who_file, newline='', mode='r', encoding='utf-8-sig') as f:
            reader = csv.reader(f)
            for i, row in enumerate(reader):
                if i == 0:
                    # First row with the name of the features
                    self._features = row
                    for feature in self._features:
                        self._features_values[feature] = []
                    continue
                
                # rows with names and values for the features
                # the name is our label we want to predict from the features
                cur_sample = row
                self.add_sample(cur_sample)

                for i, feature_val in enumerate(cur_sample):
                    cur_feature = self._features[i]
                    if feature_val not in self._features_values[cur_feature]:
                        self._features_values[cur_feature].append(feature_val)
    
    def add_sample(self, sample_vector):
        new_sample = Sample(sample_vector, self._features, self._decision_feature)
        self._samples.append(new_sample)

        return len(self._samples)-1
    
    def get_classification_features(self):
        classification_features = self._features.copy()
        classification_features.remove(self._decision_feature)

        return classification_features

    def get_decision_feature(self):
        return self._decision_feature
    
    def get_feature_values(self, feature):
        assert feature in self._features_values.keys(), "'{0}' is not a valid feature.".format(feature)

        return self._features_values[feature]
    
    def get_sample_at_index(self, index):
        assert index >= 0 and index < len(self._samples), "The parameter index must be >= 0 and less than the number of samples"

        return self._samples[index]
    
    def get_samples(self):
        return self._samples
    
    def get_values_by_feature(samples, feature):
        values = []
        for sample in samples:
            values.append(sample.get_feature_value(feature))
        
        return values
    
    def get_samples_by_feature_value(samples, feature, feature_value):
        subsamples = []
        for sample in samples:
            if sample.get_feature_value(feature) == feature_value:
                subsamples.append(sample)
        
        return subsamples
    
    def plurality_value(self, samples):
        max_count = None
        best_label = None
        labels = SamplesSet.get_values_by_feature(samples, self._decision_feature)
        for value in self._features_values[self._decision_feature]:
            cur_count = labels.count(value)
            if max_count is None or cur_count > max_count:
                max_count = cur_count
                best_label = value
        
        return best_label