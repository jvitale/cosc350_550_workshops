import random
import json

from tictactoe_game_environment import TicTacToeGameEnvironment
from reinforcement_learning import state_to_str

def agent_program_random(percepts, actuators):
    game_board = percepts['game-board-sensor']
    player_turn = percepts['turn-taking-indicator']
    game_state = {
        'game-board': game_board.copy(),
        'player-turn': player_turn
    }
    
    legal_moves = TicTacToeGameEnvironment.get_legal_actions(game_state)
    if len(legal_moves) > 0:
        return [random.choice(legal_moves)]
    
    return []

def agent_program_RL(percepts, actuators):

    game_board = percepts['game-board-sensor']
    player_turn = percepts['turn-taking-indicator']
    game_state = {
        'game-board': game_board.copy(),
        'player-turn': player_turn
    }

    with open('vfunction.json'.format(player_turn), 'r') as f:
        vfunction = json.load(f)
    
    opponent = 'X' if player_turn == 'O' else 'O'
    v_player = vfunction['player-{0}'.format(player_turn)]
    v_opponent = vfunction['player-{0}'.format(opponent)]
    if not TicTacToeGameEnvironment.is_terminal(game_state):
        best_action = None
        max_advantage = None
        for action in TicTacToeGameEnvironment.get_legal_actions(game_state):
            new_state = TicTacToeGameEnvironment.transition_result(game_state, action)
            future_state_str = state_to_str(new_state)
            if future_state_str in v_player.keys() and future_state_str in v_opponent.keys():
                advantage = v_player[future_state_str] - v_opponent[future_state_str]
                if best_action is None or advantage > max_advantage:
                    best_action = action
                    max_advantage = advantage
        
        if best_action is not None:
            return [best_action]
        else:
            print("No best action found in v function for state {0}. Selecting it randomly".format(state_to_str(game_state)))
            actions = TicTacToeGameEnvironment.get_legal_actions(game_state)
            selected_action = random.choice(actions)
            return [selected_action]
    
    return []
