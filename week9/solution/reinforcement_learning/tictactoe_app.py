from une_ai.tictactoe import TicTacToeGame
from une_ai.tictactoe import TicTacToePlayer

from tictactoe_game_environment import TicTacToeGameEnvironment
from agent_programs import agent_program_RL, agent_program_random

if __name__ == '__main__':
    player_X = TicTacToePlayer('X', agent_program_RL)
    player_O = TicTacToePlayer('O', agent_program_random)

    # DO NOT EDIT THE FOLLOWING INSTRUCTIONS!
    environment = TicTacToeGameEnvironment()
    environment.add_player(player_X)
    environment.add_player(player_O)

    game = TicTacToeGame(player_X, player_O, environment)

