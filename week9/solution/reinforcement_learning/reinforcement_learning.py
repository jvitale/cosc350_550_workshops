import random
import json
import sys

from tictactoe_game_environment import TicTacToeGameEnvironment

# code taken from https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
# A function to print a progress bar to keep track
# of the learning process
def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()

# A function transforming a game board state into a
# string representing it
def state_to_str(state):
    board_str = ''
    game_board = state['game-board']
    for i in range(game_board.get_width()):
        for j in range(game_board.get_height()):
            value = game_board.get_item_value(i, j)
            if value is None:
                value = '*'
            board_str += value
    
    return board_str

# A function that picks the best next action for the
# current player given the current policies.
# The best action for the player is determined as the
# difference between the v-value of the player
# to transition in the new state and the v-value of
# the opponent in that same transitioned state.
# There is an off_policy_likelihood percentage to
# select a random action instead of the best one.
def pick_best_action(vfunction, state, off_policy_likelihood):
    player = 'player-{0}'.format(state['player-turn'])
    opponent = 'player-X' if state['player-turn'] == 'O' else 'player-O'
    v_player = vfunction[player]
    v_opponent = vfunction[opponent]

    # getting the legal actions
    legal_actions = TicTacToeGameEnvironment.get_legal_actions(state)

    # setting the selected action as None
    selected_action = None

    # keeping track of the states not explored yet
    # (for exploration mode)
    unvisited_future_states = []

    # selecting the action based on the current policy from the
    # so far learned v function for the player
    max_advantage = float('-Inf')
    for action in legal_actions:
        future_state = TicTacToeGameEnvironment.transition_result(state, action)
        future_state_str = state_to_str(future_state)
        if future_state_str in v_player.keys() and future_state_str in v_opponent.keys():
            advantage = v_player[future_state_str] - v_opponent[future_state_str]
            if advantage > max_advantage:
                selected_action = action
                max_advantage = advantage
        elif future_state_str not in v_player.keys():
            unvisited_future_states.append((action, future_state))

    # checking if we should use exploration instead of exploitation
    if random.random() < off_policy_likelihood or selected_action is None:
        # exploration mode, selecting random action
        if len(unvisited_future_states) > 0:
            item = random.choice(unvisited_future_states)
            selected_action = item[0]
        else:
            selected_action = random.choice(legal_actions)
    
    return selected_action

# A function to update the v function of a player
# The v function is updated according to the TD(0) equation
def update_v_function(v, state, new_state, reward, alpha, gamma):
    state_str = state_to_str(state)
    if state_str not in v.keys():
        v[state_str] = random.random()*0.1
    
    future_state_str = state_to_str(new_state)
    if future_state_str not in v.keys():
        v[future_state_str] = random.random()*0.1
    
    v_old = v[state_str]
    v_new = v_old + alpha*(reward + gamma*v[future_state_str] - v_old)
    v[state_str] = v_new

# The learning function
def learn(alpha=0.1, gamma=0.9, off_policy_likelihood=0.1, n_episodes=100000):
    # setting the v functions for the players as empty dictionaries
    vfunction = {'player-X': {}, 'player-O': {}}

    # We start from the opening state of the game
    starting_environment = TicTacToeGameEnvironment()
    state = starting_environment.get_game_state()

    # We need to keep track to the last state to update for the
    # other player
    past_states = {'player-X': None, 'player-O': None}

    # Loop for n_episodes
    i = 0
    while i < n_episodes:
        progress(i, n_episodes)

        cur_player = state['player-turn']
        last_player = 'X' if state['player-turn'] == 'O' else 'O'

        v_cur_player = vfunction['player-{0}'.format(cur_player)]
        v_last_player = vfunction['player-{0}'.format(last_player)]

        # from the current state, we pick the best action
        selected_action = pick_best_action(
            vfunction,
            state,
            off_policy_likelihood
        )

        # Transitioning state
        new_state = TicTacToeGameEnvironment.transition_result(state, selected_action)

        # We store the transitioned state as the state to update for the current player
        # during the next iteration
        past_states['player-{0}'.format(state['player-turn'])] = new_state

        # We compute the reward for the current player for performing
        # the selected action and transitioning to new_state
        # If new_state is not terminal, the reward will be 0
        cur_player_reward = TicTacToeGameEnvironment.payoff(new_state, cur_player)

        # Given the received reward, we update the v function of the current player
        # Even if the reward = 0, the v function for the current state will be updated
        # given the gamma*v-value of the new_state, thus propagating the rewards from
        # terminal states back to early states of the game
        update_v_function(v_cur_player, state, new_state, cur_player_reward, alpha, gamma)

        # We also need to update the v function of the last player
        # If we have a past state for the last player, we can do so
        if past_states['player-{0}'.format(last_player)] is not None:
            last_player_new_state = past_states['player-{0}'.format(last_player)]
            
            # The reward is given by the payoff at the same new_state but for the last_player
            # In this game is simply -1*cur_player_reward
            last_player_reward = TicTacToeGameEnvironment.payoff(new_state, last_player)

            # We need to update the v-value for the past state of the last player
            # when transitioning to the new_state with the achieved reward for new_state
            update_v_function(v_last_player, last_player_new_state, new_state, last_player_reward, alpha, gamma)  

        # We check if the episode terminated
        if TicTacToeGameEnvironment.is_terminal(new_state):
            # Yes, we restart
            state = starting_environment.get_game_state()
            past_states = {'player-X': None, 'player-O': None}
            i += 1
        else:
            # No, we continue
            state = new_state
    
    return vfunction

if __name__ == "__main__":
    vfunction = learn()
    with open('vfunction.json', 'w+') as f:
        json.dump(vfunction, f)