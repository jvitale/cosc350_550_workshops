from une_ai.models import GraphNode

class DTNode(GraphNode):

    def __init__(self, state, parent_node, edge):
        super().__init__(state, parent_node, edge, 0)
        
        self._successors = {}
    
    def add_successor(self, successor, edge):
        self._successors[edge] = successor

        return self._successors[edge]
    
    def get_successors(self):
        successors = {}
        for key, val in self._successors.items():
            successors[key] = val
        return successors