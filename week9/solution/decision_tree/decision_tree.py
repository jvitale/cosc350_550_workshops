import math

from dt_node import DTNode
from guess_who_samples import SamplesSet

training_set = SamplesSet()

def entropy(samples, feature):
    global training_set

    # generating a list with all the values for the considered
    # feature given in the samples
    values = SamplesSet.get_values_by_feature(samples, feature)

    # computing entropy by summing up
    # the partial entropy for each feature value
    feature_vals = training_set.get_feature_values(feature)
    total_entropy = 0
    for value in feature_vals:
        n_positive = values.count(value)
        prob = n_positive / len(values)
        if prob == 0 or prob == 1:
            # current partial entropy is 0, skip
            continue
        total_entropy += prob*math.log2(prob)
    
    return -1*total_entropy

def remainder(samples, feature):
    global training_set

    feature_vals = training_set.get_feature_values(feature)
    total_remainder = 0
    for value in feature_vals:
        cur_samples = SamplesSet.get_samples_by_feature_value(samples, feature, value)
        
        if len(cur_samples) > 0:
            total_remainder += (len(cur_samples) / len(samples)) * entropy(cur_samples, training_set.get_decision_feature())
    
    return total_remainder

def information_gain(samples, feature):
    global training_set

    return entropy(samples, training_set.get_decision_feature()) - remainder(samples, feature)

def learn_tree(samples, features, parent_samples, parent_node=None, edge=None):
    global training_set

    if len(samples) == 0:
        leaf = training_set.plurality_value(parent_samples)
        return DTNode(leaf, parent_node, edge)
    
    labels = SamplesSet.get_values_by_feature(samples, training_set.get_decision_feature())
    if len(set(labels)) == 1:
        leaf = labels[0]
        return DTNode(leaf, parent_node, edge)
    
    if len(features) == 0:
        leaf = training_set.plurality_value(samples)
        return DTNode(leaf, parent_node, edge)

    best_feature = None
    best_ig = None
    for feature in features:
        if feature != training_set.get_decision_feature():
            cur_ig = information_gain(samples, feature)
            if best_ig is None or cur_ig > best_ig:
                best_ig = cur_ig
                best_feature = feature
    
    tree = DTNode(best_feature, parent_node, edge)
    for value in training_set.get_feature_values(best_feature):
        subsamples = SamplesSet.get_samples_by_feature_value(samples, best_feature, value)
        
        subfeatures = features.copy()
        subfeatures.remove(best_feature)
        subtree = learn_tree(subsamples, subfeatures, samples, tree, best_feature)
        tree.add_successor(subtree, value)
    
    return tree

def classify(sample, decision_tree):
    print("\n--------------------------")
    print("Classifying the sample {0}".format(sample))

    cur_node = decision_tree
    while len(cur_node.get_successors()) > 0:
        successors = cur_node.get_successors()
        cur_feat = cur_node.get_state()
        print("Evaluating feature '{0}'".format(cur_feat))
        feat_val = sample.get_feature_value(cur_feat)
        print("Feature value for the sample is '{0}'".format(feat_val))

        cur_node = successors[feat_val]

    print("Classification: {0}".format(cur_node.get_state()))

    return cur_node.get_state()

if __name__ == '__main__':
    print("Learning decision tree...")
    decision_tree = learn_tree(
        training_set.get_samples(),
        training_set.get_classification_features(),
        [], None, None)
    
    print("Decision tree learned!")

    print("Classification...\n")
    n_misclassifications = 0
    for sample in training_set.get_samples():
        ground_truth = sample.get_label()
        prediction = classify(sample, decision_tree)

        if ground_truth != prediction:
            n_misclassifications += 1
            print("Misclassification for sample {0}".format(sample))
            print("Ground truth was '{0}' and prediction was '{1}'".format(ground_truth, prediction))

    print("\nNumber of misclassifications: {0}".format(n_misclassifications))
