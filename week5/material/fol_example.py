from pyDatalog import pyDatalog

pyDatalog.create_terms('IsFemale,IsMale,AreParents,IsHungry,Want,X,Y,Z')

#+ IsFemale('Jane')
#+ IsMale('Joe')
+ IsMale('Bob')
+ AreParents('Jane','Joe','Bob')
+ IsHungry('Bob')

# adding last fact with .load method
pyDatalog.load("+ Want('Bob','Milk')")

# adding clauses (and commenting the two given facts above)
IsFemale(X) <= AreParents(X, Y, Z)
IsMale(Y) <= AreParents(X, Y, Z)

result = IsMale(X)
print(result)

# asking with .ask method
answer = pyDatalog.ask('IsMale(X) & IsHungry(X)')
print(answer)