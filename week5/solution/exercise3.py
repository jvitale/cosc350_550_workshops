KB = {
    'P->Q': lambda p, q, r, s: not p or q, # using the logic equivalence
    'not R -> not P or Q': lambda p, q, r, s: r or (not p or q),
    'S -> not Q or R': lambda p, q, r, s: not s or (not q or r)
}

QUERIES = {
    'P and S': lambda p, q, r, s: p and s,
    'not Q and not R': lambda p, q, r, s: not q and not r,
    'not P or Q': lambda p, q, r, s: not p or q
}

if __name__ == '__main__':

    # listing all models in M(KB)
    KB_models = []
    for p in [True, False]:
        for q in [True, False]:
            for r in [True, False]:
                for s in [True, False]:
                    valid = True
                    for rule in KB.values():
                        # Testing if current model
                        # makes KB rules true
                        # If only one rules is not true
                        # then set not valid and break
                        if not rule(p, q, r, s):
                            valid = False
                            break
                    # if a valid model, save it
                    if valid:
                        KB_models.append({
                            'P': p,
                            'Q': q,
                            'R': r,
                            'S': s
                        })
    # listing the models in M(KB)
    print("Models in M(KB):")
    for m in KB_models:
        print(m)
    
    # testing queries
    for query, query_lambda in QUERIES.items():
        valid = True
        for m in KB_models:
            if not query_lambda(m['P'], m['Q'], m['R'], m['S']):
                valid = False
                print("The query '{0}' is not entailed by the KB: for P={1}, Q={2}, R={3}, S={4} KB is True but query is not".format(query, m['P'], m['Q'], m['R'], m['S']))
                break
        if valid:
            print("The query '{0}' is  entailed by the KB".format(query))
