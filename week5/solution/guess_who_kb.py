import csv
from pyDatalog import pyDatalog

# KNOWLEDGE BASE

# loading the features of the guess who characters from the csv
names = []
with open('guess_who.csv', newline='', mode='r', encoding='utf-8-sig') as f:
    reader = csv.reader(f)
    properties = None
    for i, row in enumerate(reader):
        if i == 0:
            # First row with the predicates
            # create the terms to use (variables and predicates)
            properties = row[1:]
            predicates = ','.join(row[1:])
            pyDatalog.create_terms('{0},X,Y,Z'.format(predicates))
            continue
        
        # rows with names and values for the predicates
        name = row[0]
        names.append(name)
        for idx, propval in enumerate(row[1:]):
            # add a fact for each predicate of the character
            pyDatalog.load("+ {0}('{1}', '{2}')".format(properties[idx], name, propval))