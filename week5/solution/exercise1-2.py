# Exercise 1

QUERIES = {
    'alpha1': (lambda p, q, z: not (p or q) and z, ['P', 'Q', 'Z']),
    'alpha2': (lambda p, q, z: (not p and q) or z, ['P', 'Q', 'Z']), # using logic equivalence
    'alpha3': (lambda p, q, z: (p or q) == q, ['P', 'Q']),
    'alpha4': (lambda p, q, z: (p and q) or (p or q) and not(p and z), ['P', 'Q', 'Z']),
    'alpha5': (lambda p, q, z: p and not p, ['P'])
}

for query, query_val in QUERIES.items():
    query_lambda, propsym = query_val
    print("")
    print("Models of M({0}):".format(query))
    models = []
    # list all models
    for p in [True, False]:
        for q in [True, False]:
            for z in [True, False]:
                if query_lambda(p, q, z):
                    model = {}
                    for sym in propsym:
                        model[sym] = eval(sym.lower())

                    if model not in models:        
                        models.append(model)
                        print(model)

"""
Exercise 2:
Check the output from the script for exercise 1 for the models in M(alpha<N>).

1. For all models of alpha2 we have that Q OR Z is True, so alpha2 |= Q OR Z
2. For P = False and Q = True in M(alpha3), P <=> Q is False, so alpha3 does not entail P <=> Q
3. The sentence P => (Q OR NOT Z) is logically equivalent to NOT P OR (Q OR NOT Z). 
For P = False, the sentence is always True.
For P = True the sentence is True when Q is True or Z is False. All the remaining models in M(alpha4) satisfy that.
So, alpha4 |= P => (Q OR NOT Z)
"""