import csv
import random
from pyDatalog import pyDatalog
import guess_who_kb

def find_best_question(remaining_characters, remaining_predicates):
    best_question = None
    max_avg_eliminated = 0
    for predicate in remaining_predicates.keys():
        for value in remaining_predicates[predicate]:
            answer = pyDatalog.ask("{0}(X, '{1}')".format(predicate, value))

            # characters I will be able to eliminate if answer is NO
            names = [r[0] for r in answer.answers if r[0] in remaining_characters]
            n_eliminated_no = len(names)
            # whereas if answer is YES
            n_eliminated_yes = len(remaining_characters) - len(names)

            # we need to make sure that the question is not a silly one
            # silly questions are questions that we already know the answer
            # because all the remaining characters share the same common feature
            # These questions lead to eliminating all the characters if the answer is YES or NO
            if n_eliminated_no == len(remaining_characters) or n_eliminated_yes == len(remaining_characters):
                continue

            n_avg_eliminated = (n_eliminated_no + n_eliminated_yes)/2
            if best_question is None or n_avg_eliminated > max_avg_eliminated:
                best_question = (predicate, value)
                max_avg_eliminated = n_avg_eliminated
    
    return best_question

    
if __name__ == '__main__':
    # MAIN APPLICATION

    names_answer = pyDatalog.ask("Gender(X, Y)")
    names = []
    for item in names_answer.answers:
        names.append(item[0])
    
    with open('guess_who.csv', newline='', mode='r', encoding='utf-8-sig') as f:
        reader = csv.reader(f)
        properties = None
        for i, row in enumerate(reader):
            if i == 0:
                # First row with the predicates
                properties = row[1:]
                break

    # selecting a character randomly that the AI agent would need to guess
    character_name = random.choice(names)
    print("The AI agent will need to guess the character with name '{0}'".format(character_name))

    # start with all the possible characters
    available_characters = names.copy()

    # start with all the predicates we can use for questions
    available_predicates = {}
    for prop in properties:
        query_outputs = pyDatalog.ask("{0}(X, Y)".format(prop))
        predicate_values = []
        for output in query_outputs.answers:
            if output[1] not in predicate_values:
                predicate_values.append(output[1])
        available_predicates[prop] = predicate_values
    
    # main body
    print("AI starts the game with the following names on the board:")
    print(available_characters)

    # The game stops when we have only one character available or we have no more questions to ask
    while len(available_characters) > 1 and len(available_predicates.keys()) > 0:
        # looking for the best next question
        best_question = find_best_question(available_characters, available_predicates)
        if best_question is None:
            print("Function find_best_question not implemented. Asking a random question")
            predicate = random.choice(list(available_predicates.keys()))
            value = random.choice(available_predicates[predicate])
            best_question = (predicate, value)

        question = "{0}(X, '{1}')".format(best_question[0], best_question[1])
        print("AI Asking query: {0}".format(question))
        # retrieving the names with the considered feature
        names_with_feature = [r[0] for r in pyDatalog.ask(question).answers if r[0] in available_characters]

        # checking if answer to the question is YES or NO
        yes_no_answer = pyDatalog.ask("{0}('{1}','{2}')".format(best_question[0], character_name, best_question[1]))
        yes_no_answer = "YES" if yes_no_answer is not None else "NO"
        print("Opponent answering: The answer is {0}".format(yes_no_answer))

        # removing predicate
        available_predicates[best_question[0]].remove(best_question[1])
        # if there is less than 2 options or the answer was YES, we can remove the predicate completely
        if yes_no_answer == "YES" or len(available_predicates[best_question[0]]) < 2:
            del available_predicates[best_question[0]]
        
        # updating the remaining characters on the board
        if yes_no_answer == "YES":
            available_characters = names_with_feature.copy()
        else:
            for name in names_with_feature:
                available_characters.remove(name)
        
        print("AI updates the remaining names on the board. Remaining names:")
        print(available_characters)
    
    # making a guess
    if len(available_characters) == 1:
        # we are sure of the guess
        print("AI says: The hidden character is {0}".format(available_characters[0]))
    else:
        # we are not sure, random guess
        print("AI says: I am not sure who the character is. I will make a guess.")
        guess = random.choice(available_characters)
        print("AI says: The hidden character is {0}".format(guess))