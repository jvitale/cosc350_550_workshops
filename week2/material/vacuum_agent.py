from une_ai.models import Agent

class VacuumAgent(Agent):

    WHEELS_DIRECTIONS = ['north', 'south', 'west', 'east']

    def __init__(self, agent_program):
        super().__init__(
            agent_name='vacuum_agent',
            agent_program=agent_program
        )
        
    # TODO: add all the sensors
    def add_all_sensors(self):
        pass
    
    # TODO: add all the actuators
    def add_all_actuators(self):
        pass

    # TODO: add all the actions
    def add_all_actions(self):
        pass

    # TODO: implement the following methods

    def get_pos_x(self):
        # It must return the x coord of the agent 
        # based on the location-sensor value
        pass
    
    def get_pos_y(self):
        # It must return the y coord of the agent 
        # based on the location-sensor value
        pass
    
    def get_battery_level(self):
        # It must return the rounded (as int) sensory value 
        # from the sensor battery-level
        pass
    
    def is_out_of_charge(self):
        # It must return True if the sensor battery-level
        # is 0 and False otherwise
        pass
    
    def collision_detected(self):
        # It must return the direction of the bumper
        # sensor collided with a wall if any, or None otherwise
        pass

    # This function is already implemented
    # so you do not need to change it
    def did_collide(self):
        return False if self.collision_detected() is None else True