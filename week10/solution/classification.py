import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

# Generate random samples for three classes distributed
# according to three gaussian distributions
def generate_samples(num_samples, mean1, cov1, mean2, cov2, mean3, cov3):
    class_labels = np.random.randint(3, size=num_samples)  # 0 , 1, 2
    
    X = []
    y = []
    for label in class_labels:
        if label == 0:
            sample = np.random.multivariate_normal(mean1, cov1)
        elif label == 1:
            sample = np.random.multivariate_normal(mean2, cov2)
        elif label == 2:
            sample = np.random.multivariate_normal(mean3, cov3)
        X.append(sample)
        y.append(label)
    
    return np.array(X), np.array(y)

def create_ann(input_size, hidden_size, output_size):
    np.random.seed(0) # random seed for replicability
    model = {
        'W1': np.random.randn(input_size, hidden_size), # weights input layer
        'b1': np.zeros((1, hidden_size)), # biases input layer
        'W2': np.random.rand(hidden_size, output_size), # weights output layer
        'b2': np.zeros((1, output_size)) # biases output layer
    }

    return model

# Sigmoid activation function and its derivative
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# The derivative is in the form of accepting the activation
# a = sigma(x)
def sigmoid_derivative(a):
    return a * (1 - a)

# One-hot encode the target labels
def one_hot_encode(labels, num_classes):
    encoded = np.zeros((len(labels), num_classes))
    encoded[np.arange(len(labels)), labels] = 1
    return encoded

# Forward propagation
def forward_propagation(X, model):
    hidden_output = sigmoid(np.dot(X, model['W1']) + model['b1'])
    output = sigmoid(np.dot(hidden_output, model['W2']) + model['b2'])

    cache = {
        'A1': hidden_output,
        'A2': output
    }

    return output, cache

# Backpropagation
def backpropagation(y, model, cache, learning_rate):
    predicted_output = cache['A2']

    # Compute loss and error
    loss = np.mean((y - predicted_output) ** 2)
    output_error = (-2/y.shape[0]) * (y - predicted_output)
    
    # Backpropagation
    output_delta = output_error * sigmoid_derivative(predicted_output)
    hidden_error = output_delta.dot(model['W2'].T) # dot product to backpropagate the error back
    hidden_output = cache['A1'] # activations of the hidden layer
    hidden_delta = hidden_error * sigmoid_derivative(hidden_output)
    
    # Update weights and biases
    model['W2'] -= hidden_output.T.dot(output_delta) * learning_rate
    model['b2'] -= np.sum(output_delta, axis=0, keepdims=True) * learning_rate
    model['W1'] -= X_train_scaled.T.dot(hidden_delta) * learning_rate
    model['b1'] -= np.sum(hidden_delta, axis=0, keepdims=True) * learning_rate

    return loss # no need to return the model, we are already updating it by reference

def train(X, y, model, num_epochs=10000, learning_rate=0.1):

    plt.ion()  # Turn on interactive mode for live updating

    # Creating a plot to show the learning process
    fig, ax = plt.subplots()
    ax.set_xlabel('Feature 1')
    ax.set_ylabel('Feature 2')
    sc0 = ax.scatter([], [], label='Class 0')
    sc1 = ax.scatter([], [], label='Class 1')
    sc2 = ax.scatter([], [], label='Class 2')
    ax.legend()
    ax.set_xlim(-4,4)
    ax.set_ylim(-4,4)

    for epoch in range(num_epochs):
        # Forward propagation
        predicted_output, cache = forward_propagation(X, model)

        # Backpropagation
        loss = backpropagation(y, model, cache, learning_rate)
        
        if epoch < 10 or epoch % 100 == 0:
            print(f"Epoch {epoch}, Loss: {loss:.4f}")

            # Update the plot
            y_pred_classes = np.argmax(predicted_output, axis=1)
            sc0.set_offsets(X[y_pred_classes == 0, :2])
            sc1.set_offsets(X[y_pred_classes == 1, :2])
            sc2.set_offsets(X[y_pred_classes == 2, :2])
            ax.relim()
            ax.autoscale_view()
            fig.canvas.flush_events()
    
    plt.ioff()
    plt.show()

if __name__ == '__main__':

    # Generating the training and test samples
    mean1 = [2, 2]
    cov1 = [[0.2, 0.1], [0.1, 0.2]]
    mean2 = [4, 4]
    cov2 = [[0.2, -0.1], [-0.1, 0.2]]
    mean3 = [6, 2]
    cov3 = [[0.5, -0.2], [0.1, -0.1]]

    num_samples = 1000
    X, y = generate_samples(num_samples, mean1, cov1, mean2, cov2, mean3, cov3)

    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    # Normalize the input features using StandardScaler
    scaler_input = StandardScaler()
    X_train_scaled = scaler_input.fit_transform(X_train)
    X_test_scaled = scaler_input.transform(X_test)

    # We need to encode the labels in classes
    num_classes = 3
    y_train_encoded = one_hot_encode(y_train, num_classes)
    y_test_encoded = one_hot_encode(y_test, num_classes)

    # Neural network architecture
    input_size = X_train_scaled.shape[1]
    hidden_size = 4 # you can change this parameter to see what happens
    output_size = num_classes 
    model = create_ann(input_size, hidden_size, output_size)

    # Training the model
    n_epochs = 10000
    learning_rate = 0.1
    train(X_train_scaled, y_train_encoded, model, n_epochs, learning_rate)

    # Testing the performance of the model
    # Prediction on test data
    y_pred, _ = forward_propagation(X_test_scaled, model)
    y_pred_classes = np.argmax(y_pred, axis=1)

    # Now we can compute the classification accuracy
    accuracy = accuracy_score(y_test, y_pred_classes)
    print(f"Accuracy on Test Data: {accuracy:.4f}")