import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt

# Generate random samples of a function with 3 inputs and 1 output
def generate_samples(num_samples):
    X = np.random.rand(num_samples, 3)
    y = np.sin(X[:, 0]) + 2 * np.cos(X[:, 1]) - 0.5 * X[:, 2]
    y = y.reshape((y.shape[0], 1))
    return X, y

def create_ann(input_size, hidden_size, output_size):
    np.random.seed(0) # random seed for replicability
    model = {
        'W1': np.random.randn(input_size, hidden_size), # weights input layer
        'b1': np.zeros((1, hidden_size)), # biases input layer
        'W2': np.random.rand(hidden_size, output_size), # weights output layer
        'b2': np.zeros((1, output_size)) # biases output layer
    }

    return model

# Sigmoid activation function and its derivative
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# The derivative is in the form of accepting the activation
# a = sigma(x)
def sigmoid_derivative(a):
    return a * (1 - a)

# Forward propagation
def forward_propagation(X, model):
    # Compute the activation of the hidden layer
    # sigmoid(dotproduct(X, W1) + b1)
    # numpy provides the method .dot for the dot product
    hidden_activation = ...

    # Now compute the output by performing the dot
    # product between the activation of the hidden layer
    # and the weights W2, sum b2 and use the sigmoid for the activation
    output = ...

    # store the activation of the hidden layer and the output layer in
    # the following dictionary
    cache = {
        'A0': X,
        'A1': ...,
        'A2': ...
    }

    return output, cache

# Backpropagation
def backpropagation(y, model, cache, learning_rate):
    predicted_output = cache['A2']

    # Compute loss and error
    # The loss is the ground truth y - the predicted output, all squared and averaged
    # numpy offers the method .mean to compute the average
    loss = ...
    
    # the output error gradient:
    # -2/num_of_samples * (ground truth - prediction)
    output_error = ...
    
    # Backpropagation
    # delta for the output layer:
    # output_error * sigmoid_derivative(prediction)
    output_delta = ...

    # hidden layer error:
    # dot product between W2 transposed (you can use .T) and the output_delta
    hidden_error = ...

    # hidden layer cached activation
    hidden_output = cache['A1'] 

    # final hidden layer delta:
    # hidden_error * sigmoid_derivative(hidden_output)
    hidden_delta = ...
    
    # Update weights and biases

    model['W2'] -= ...
    model['b2'] -= ...
    model['W1'] -= ...
    model['b1'] -= ...

    return loss # no need to return the model, we are already updating it by reference

def train(X, y, model, num_epochs=10000, learning_rate=0.1):

    plt.ion()  # Turn on interactive mode for live updating

    # Creating a plot to show the learning process
    fig, ax = plt.subplots()
    ax.set_xlabel('Training sample')
    ax.set_ylabel('Y')
    line_gt, = ax.plot([], [], label='Ground truth')
    line_pred, = ax.plot([], [], label='Prediction')
    ax.legend()

    for epoch in range(num_epochs):
        # Forward propagation
        predicted_output, cache = forward_propagation(X, model)

        # Backpropagation
        loss = backpropagation(y, model, cache, learning_rate)
        
        if epoch % 100 == 0:
            print(f"Epoch {epoch}, Loss: {loss:.4f}")

            # Update the plot
            line_gt.set_data(range(y.shape[0]), y) # ground truth
            line_pred.set_data(range(predicted_output.shape[0]), predicted_output) # predictions
            ax.relim()
            ax.autoscale_view()
            fig.canvas.flush_events()
    
    plt.ioff()
    plt.show()

if __name__ == '__main__':

    # Generating the training and test samples
    num_samples = 1000
    X, y = generate_samples(num_samples)

    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    # Normalize the input features using StandardScaler
    scaler_input = StandardScaler()
    X_train_scaled = scaler_input.fit_transform(X_train)
    X_test_scaled = scaler_input.transform(X_test)

    # We are trying to learn a function with co-domain not bounded
    # by [0, 1]. The used activation function (Sigmoid)
    # only outputs values between 0 and 1 so we need to make sure
    # that we scale the outputs between 0 and 1 as well
    scaler_output = MinMaxScaler()
    y_train_scaled = scaler_output.fit_transform(y_train)
    y_test_scaled = scaler_output.transform(y_test)

    # Neural network architecture
    input_size = X_train_scaled.shape[1]
    hidden_size = 8 # you can change this parameter to see what happens
    output_size = 1 
    model = create_ann(input_size, hidden_size, output_size)

    # Training the model
    n_epochs = 10000
    learning_rate = 0.1
    train(X_train_scaled, y_train_scaled, model, n_epochs, learning_rate)

    # Testing the performance of the model
    # Prediction on test data
    y_pred_scaled, _ = forward_propagation(X_test_scaled, model)

    # We need to scale back the predictions
    y_pred = scaler_output.inverse_transform(y_pred_scaled)

    # Now we can compute the mean squared error
    mse = mean_squared_error(y_test, y_pred)
    print(f"\nMSE for the test set: {mse:.4f}")