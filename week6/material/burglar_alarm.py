from pgmpy.models import BayesianNetwork
from pgmpy.factors.discrete.CPD import TabularCPD

# Let's create the Bayesian network model
burglar_alarm_bn = BayesianNetwork()

# Adding the nodes to the network
burglar_alarm_bn.add_nodes_from(['Burglary', 'Earthquake', 'Alarm', 'JohnCalls', 'MaryCalls'])

# Adding dependencies
burglar_alarm_bn.add_edge('Burglary', 'Alarm')
burglar_alarm_bn.add_edge('Earthquake', 'Alarm')
burglar_alarm_bn.add_edge('Alarm', 'JohnCalls')
burglar_alarm_bn.add_edge('Alarm', 'MaryCalls')

# Conditional probability tables

burglary_cpd = TabularCPD(
    variable='Burglary',
    variable_card=2,
    values=[[0.001], [0.999]]
)

eq_cpd = TabularCPD(
    variable='Earthquake',
    variable_card=2,
    values=[[0.002], [0.998]]
)

alarm_cpd = TabularCPD(
    variable='Alarm',
    variable_card=2,
    evidence=['Burglary', 'Earthquake'],
    evidence_card=[2, 2],
    values=[
        # B=true^E=true, B=true^E=false, B=false^E=true, B=false^E=false
        [0.95, 0.94, 0.29, 0.001], # Alarm true
        [0.05, 0.06, 0.71, 0.999] # Alarm false
    ]
)

jc_cpd = TabularCPD(
    variable='JohnCalls',
    variable_card=2,
    evidence=['Alarm'],
    evidence_card=[2],
    values=[
        # Alarm=true, Alarm=false
        [0.9, 0.05], # JC true
        [0.1, 0.95] # JC false
    ]
)

mc_cpd = TabularCPD(
    variable='MaryCalls',
    variable_card=2,
    evidence=['Alarm'],
    evidence_card=[2],
    values=[
        # Alarm=true, Alarm=false
        [0.7, 0.01], # MC true
        [0.3, 0.99] # MC false
    ]
)

burglar_alarm_bn.add_cpds(burglary_cpd, eq_cpd, alarm_cpd, jc_cpd, mc_cpd)

# checking that everything is ok
try:
    model_check = burglar_alarm_bn.check_model()
except Exception as e:
    model_check = e

if model_check == True:
    print("The Bayesian network model does not have any error")
else:
    print("Warning! There is an error in the Bayesian network model: {0}".format(e))

# Probabilistic inference
from pgmpy.inference import VariableElimination

inference = VariableElimination(burglar_alarm_bn)

# Probability of Alarm being on or off
query = inference.query(variables=['Alarm'])
print("What is the probability for the alarm to become activated?")
print(query)

# Probability of Alarm being on or off given JohnCalls=true
query = inference.query(variables=['Alarm'], evidence={'JohnCalls': 0})
print("What is the probability for the alarm to become activated given that John called?")
print(query)

# Probability of Burglary and Alarm given JohnCalls=true
query = inference.query(variables=['Burglary','Alarm'], evidence={'JohnCalls': 0})
print("What is the probability of a burglar in the apartment and the alarm to be active given that John called?")
print(query)

# MAP query
query = inference.map_query(variables=['JohnCalls', 'MaryCalls'], evidence={'Burglary': 0})
print("Who would more likely call us when a burglar is in our house?")
print(query)