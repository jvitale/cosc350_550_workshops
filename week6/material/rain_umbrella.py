from pomegranate.distributions import Categorical
from pomegranate.hmm import DenseHMM
import numpy as np

model = DenseHMM()

states = ['rain', 'not_rain']

# emission probability distributions for the two hidden states
rain = Categorical([[0.9, 0.1]]) # 90% chances of seeing the umbrella if it's raining
not_rain = Categorical([[0.2, 0.8]]) # 20% chances of seeing the umbrella if it's not raining
model.add_distributions([rain, not_rain])

# Initial probabilities for state X0
model.add_edge(model.start, rain, 0.5)
model.add_edge(model.start, not_rain, 0.5)

# Transition probabilities
model.add_edge(rain, rain, 0.7)
model.add_edge(rain, not_rain, 0.3)
model.add_edge(not_rain, rain, 0.3)
model.add_edge(not_rain, not_rain, 0.7)

# Using the following observations
observations = ['umbrella', 'umbrella', 'not_umbrella', 'umbrella', 'not_umbrella', 'not_umbrella', 'not_umbrella']
X = np.array([[[['umbrella', 'not_umbrella'].index(label)] for label in observations]])

# Making a prediction of the most likely sequence of states that generated the observations
y_hat = model.predict(X)
predictions = [states[y] for y in y_hat[0].tolist()]

# Results
print("Observations: {}".format(observations))
print("hmm pred: {}".format(predictions))