from pomegranate.distributions import Categorical
from pomegranate.hmm import DenseHMM
import numpy as np

from pgmpy.inference import VariableElimination
from exercise3 import hogwarts_bn

inference = VariableElimination(hogwarts_bn)

hogwarts_hmm = DenseHMM()

states = ['gryffindor', 'ravenclaw', 'slytherin', 'hufflepuff']

# Possible observations: 
# 0. AcromantulaSighting=true, UnicornSighting=true (XX)
# 1. AcromantulaSighting=true, UnicornSighting=false (XO)
# 2. AcromantulaSighting=false, UnicornSighting=true (OX)
# 4. AcromantulaSighting=false, UnicornSighting=false (OO)

# emission probability distributions for the two hidden states
state_dists = [None, None, None, None]

for i, house in enumerate(states):
    query = inference.query(variables=['AcromantulaSighting','UnicornSighting'], evidence={'House': i, 'TimeOfDay': 1} )
    val = query.values
    probabilities = [val[0,0], val[0,1], val[1,0], val[1,1]]
    cur_state_dist = Categorical([probabilities])
    state_dists[i] = cur_state_dist

hogwarts_hmm.add_distributions(state_dists)

# Transition probabilities
# 25% uniform chances for a student to be allocated to a house

for i in range(4):
    hogwarts_hmm.add_edge(hogwarts_hmm.start, state_dists[i], 0.25)

# and then it is impossible to change house, so we keep the chances very low
for i in range(4):
    for j in range(4):
        if i == j:
            prob = 0.997
        else:
            prob = 0.001
        hogwarts_hmm.add_edge(state_dists[i],state_dists[j], prob)


# Using the following observations
observations = ['00', '00', '00', 'X0', '00', '00', 'X0', '00', '0X', '00']
X = np.array([[[['XX', 'X0', '0X', '00'].index(label)] for label in observations]])

# Making a prediction of the most likely sequence of states that generated the observations
y_hat = hogwarts_hmm.predict(X)
predictions = [states[y] for y in y_hat[0].tolist()]

# Results
print("Observations: {}".format(observations))
print("hmm pred: {}".format(predictions))