from pgmpy.inference import VariableElimination
from exercise3 import hogwarts_bn

inference = VariableElimination(hogwarts_bn)

# Question 1
query = inference.query(variables=['PureHeart'])
print("1. What are the chances of a Hogwarts' student to posses a pure heart?")
print(query)

# Question 2
query = inference.query(variables=['UnicornSighting'])
print("2. What is the likelihood of spotting a unicorn in the Forbidden Forest?")
print(query)

# Question 3
query = inference.query(variables=['Brave'])
print("3. What is the likelihood for a Hogwarts' student to be brave?")
print(query)

# Question 4
query = inference.query(variables=['House'], evidence={'Brave': 0})
print("4. What are the chances that a brave student comes from the Gryffindor house?")
print(query)

# Question 5
query = inference.query(variables=['AcromantulaSighting'])
print("5. What are the chances of seeing an acromantula in the Forbidden Forest?")
print(query)

# Question 6
query = inference.query(variables=['AcromantulaSighting'], evidence={'TimeOfDay': 1})
print("6. How likely is it to spot an acromantula at night?")
print(query)

# Question 7
query = inference.query(variables=['AcromantulaSighting'], evidence={'Brave': 0})
print("7. What are the chances for a brave student to encounter an acromantula?")
print(query)

# Question 8
query = inference.query(variables=['AcromantulaSighting'], evidence={'House': 1})
print("8. What is the likelihood of encountering an acromantula for a Ravenclaw student?")
print(query)

# Question 9
temp = inference.query(variables=['AcromantulaSighting'], evidence={'TimeOfDay':1, 'House': 1})
print("9. To what extent the likelihood of encountering an acromantula for a Ravenclaw student increases at night?")
print("It increases of ", temp.values[0]/query.values[0], " times")

# Question 10
query = inference.query(variables=['AcromantulaSighting'], evidence={'TimeOfDay':1, 'House': 0})
print("10. What is the likelihood for Harry Potter to encounter an acromantula when sneaking out from his dormroom during the night?")
print(query)

# Question 11
query = inference.map_query(variables=['AcromantulaSighting', 'UnicornSighting'], evidence={'TimeOfDay': 1, 'House': 2})
print("11. Consider Draco Malfoy sneaking out to go Forbidden Forest at night. Which scenario most likely happen? a. He will spot an acromantula and a unicorn, b. He will spot an acromantula only, c. He will spot a unicorn only, d. He will not spot any creature.")
print(query)

# Question 12
query = inference.map_query(variables=['House'], evidence={'Brave': 0, 'PureHeart': 0})
print("12. To which house a brave and pure hearted student is more likely to be allocated by the Sorting Hat?")
print(query)
