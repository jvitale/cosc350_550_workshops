"""
Exercise 1:

From the scenario we can identify the following RVs:

SpottingUnicorn, PureHeartWizard, SpottingTarantula, TimeOfDay, BraveWizard, WizardHouse

Now we can specify the ranges for these RVs:

UnicornSighting: {true, false}
PureHeart: {true, false}
AcromantulaSighting: {true, false}
TimeOfDay: {day, night}
House: {hufflepuff, gryffindor, ravenclaw, slytherin}
Brave: {true, false}

Dependencies:

UnicornSighting depends on PureHeart
PureHeart depends on House
AcromantulaSighting depends on TimeOfDay and Brave
Brave depends on House

We can specify the following probability distributions given by the scenario:
"""

pd = {}

pd['UnicornSighting=true|PureHeart=false'] = 1/400
pd['UnicornSighting=false|PureHeart=false'] = 1 - pd['UnicornSighting=true|PureHeart=false']
pd['UnicornSighting=true|PureHeart=true'] = 0.1
pd['UnicornSighting=false|PureHeart=true'] = 1 - pd['UnicornSighting=true|PureHeart=true']
pd['TimeOfDay=day'] = 0.5 # assuming uniform distribution
pd['TimeOfDay=night'] = 0.5 # assuming uniform distribution

probs = [0.01, 0.025, 0.05, 0.1]
i = 0
for t in ['day', 'night']:
    for brave in ['true', 'false']:
        pd['AcromantulaSighting=true|TimeOfDay={0},Brave={1}'.format(t, brave)] = probs[i]
        pd['AcromantulaSighting=false|TimeOfDay={0},Brave={1}'.format(t, brave)] = 1 - probs[i]
        i += 1

houses = ['gryffindor', 'ravenclaw', 'slytherin', 'hufflepuff']
for i, prob in enumerate([0.002, 0.002, 0.0001, 0.01]):
    pd['PureHeart=true|House={0}'.format(houses[i])] = prob
    pd['PureHeart=false|House={0}'.format(houses[i])] = 1 - prob
for i, prob in enumerate([0.05, 0.02, 0.02, 0.01]):
    pd['Brave=true|House={0}'.format(houses[i])] = prob
    pd['Brave=false|House={0}'.format(houses[i])] = 1 - prob
for house in houses:
    pd['House={0}'.format(house)] = 0.25

print("\n>>>Given probability distributions:")
for key, value in pd.items():
    print('P({0}) = {1}'.format(key, value))

"""
Exercise 2: Inference

Now we can proceed to infer new information from the one available.
"""

print('\n>>>Inferred probability distributions:')

"""
1. Computing P(PureHeart):

We can find thid distribution by marginalising the joint distribution P(PureHeart, House):

P(PureHeart) = sum_h P(PureHeart, House)
= sum_h P(PureHeart | House)P(House)

"""
for heart in ['true', 'false']:
    cur_key = 'PureHeart={0}'.format(heart)
    pd[cur_key] = 0
    for house in houses:
        pd[cur_key] += pd['PureHeart={0}|House={1}'.format(heart, house)]*pd['House={0}'.format(house)]
    
    print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

"""
2. Computing P(UnicornSighting):

We can estimate the prior by marginalising the joint distribution P(UnicornSighting, PureHeart):

P(UnicornSighting) = sum_h P(UnicornSighting, PureHeart = h)
= sum_h P(UnicornSighting | PureHeart = h)P(Heart = h)
"""
for unicorn in ['true', 'false']:
    cur_key = 'UnicornSighting={0}'.format(unicorn)
    pd[cur_key] = 0
    for heart in ['true', 'false']:
        pd[cur_key] += pd['UnicornSighting={0}|PureHeart={1}'.format(unicorn, heart)]*pd['PureHeart={0}'.format(heart)]
    print('P({0}) = {1}'.format(cur_key, pd[cur_key]))
"""

3. Computing P(Brave):

We can marginalise this variable from the joint distribution

P(Brave, House) = P(Brave | House)P(House)

so

P(Brave) = sum_h P(Brave | House = h)P(House = h)
"""

pd['Brave=true'] = 0
pd['Brave=false'] = 0

for house in houses:
    for brave in ['true', 'false']:
        pd['Brave={0}'.format(brave)] += pd['Brave={0}|House={1}'.format(brave, house)]*pd['House={0}'.format(house)]

print("P(Brave=true) = {0}".format(pd['Brave=true']))
print("P(Brave=false) = {0}".format(pd['Brave=false']))

"""
4. Computing P(House | Brave):

Given the Bayes' rule,

P(House | Brave) = P(Brave | House)P(House) / P(Brave)

Therefore
"""

for house in houses:
    for brave in ['true', 'false']:
        cur_key = 'House={0}|Brave={1}'.format(house, brave)
        pd[cur_key] = (pd['Brave={0}|House={1}'.format(brave,house)]*pd['House={0}'.format(house)])/pd['Brave={0}'.format(brave)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

"""
5. Computing P(AcromantulaSighting):

We can marginalise AcromantulaSighting from the joint distribution P(AcromantulaSighting, TimeOfDay, Brave)

P(A) = sum_t sum_b P(A, T=t, B=b) = sum_t sum_b P(A | T=t, B=b)P(T=t | B=b)P(B=b)

TimeOfDay is independent from Brave because Brave is a non-descentent of TimeOfDay and
TimeOfDay does not have parents, therefore we can simplify the formula:

P(A) = sum_t sum_b P(A | T=t, B=b)P(T=t)P(B=b)

"""
for acromantula in ['true', 'false']:
    cur_key = 'AcromantulaSighting={0}'.format(acromantula)
    pd[cur_key] = 0
    for t in ['day', 'night']:
        for brave in ['true', 'false']:
            pd[cur_key] += pd['AcromantulaSighting={0}|TimeOfDay={1},Brave={2}'.format(acromantula, t, brave)]*pd['TimeOfDay={0}'.format(t)]*pd["Brave={0}".format(brave)]
    print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

"""
6. Computing P(AcromantulaSighting | TimeOfDay):

As per the Bayes' rule

P(AcromantulaSighiting | TimeOfDay) = P(AcromantulaSighting, TimeOfDay) / P(TimeOfDay)

We can compute P(AcromantulaSighting, TimeOfDay) by marginalising the joint distribution

P(AcromantulaSighting, TimeOfDay, Brave) = P(AcromantulaSighting | TimeOfDay, Brave)P(TimeOfDay)P(Brave)

P(AcromantulaSighting, TimeOfDay) = sum_b P(AcromantulaSighting | TimeOfDay, Brave=b)P(TimeOfDay)P(Brave=b)
"""

for acromantula in ['true', 'false']:
    for t in ['day', 'night']:
        cur_key = 'AcromantulaSighting={0},TimeOfDay={1}'.format(acromantula, t)
        pd[cur_key] = 0
        for brave in ['true', 'false']:
            pd[cur_key] += pd['AcromantulaSighting={0}|TimeOfDay={1},Brave={2}'.format(acromantula, t, brave)]*pd['TimeOfDay={0}'.format(t)]*pd['Brave={0}'.format(brave)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))
"""
Now we can plug this in and compute the posteriori
"""
for acromantula in ['true', 'false']:
    for t in ['day', 'night']:
        cur_key = 'AcromantulaSighting={0}|TimeOfDay={1}'.format(acromantula, t)
        pd[cur_key] = pd['AcromantulaSighting={0},TimeOfDay={1}'.format(acromantula, t)]/pd['TimeOfDay={0}'.format(t)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))
"""
7. Computing P(AcromantulaSighting | Brave):

As per the Bayes' rule

P(AcromantulaSighting | Brave) = P(AcromantulaSighting, Brave) / P(Brave)

We can compute P(AcromantulaSighting, Brave) by marginalising P(AcromantulaSighting, TimeOfDay, Brave)

P(AcromantulaSighting, Brave) = sum_t P(AcromantulaSighting, TimeOfDay=t, Brave)
P(AcromantulaSighting, Brave) = sum_t P(AcromantulaSighting | TimeOfDay=t, Brave)P(TimeOfDay = t)P(Brave)
"""
for acromantula in ['true', 'false']:
    for brave in ['true', 'false']:
        cur_key = 'AcromantulaSighting={0},Brave={1}'.format(acromantula, brave)
        pd[cur_key] = 0
        for t in ['day', 'night']:
            pd[cur_key] += pd['AcromantulaSighting={0}|TimeOfDay={1},Brave={2}'.format(acromantula, t, brave)]*pd['TimeOfDay={0}'.format(t)]*pd['Brave={0}'.format(brave)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))
"""
Now we can plug it in the bayes rule
"""
for acromantula in ['true', 'false']:
    for brave in ['true', 'false']:
        cur_key = 'AcromantulaSighting={0}|Brave={1}'.format(acromantula, brave)
        pd[cur_key] = pd['AcromantulaSighting={0},Brave={1}'.format(acromantula, brave)]/pd['Brave={0}'.format(brave)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

"""
8. Computing P(AcromantulaSighting | House):

P(A | H) = P(A, H) / P(H)

P(A, H) = sum_b P(A, B=b, H) = sum_b P(A | B=b, H)P(B=b| H)P(H)

In this scenario, A is conditionally independent from H given B, in fact, if we know that a student is brave, we don't
need to know their house to infer the likelihood of spotting an acromantula. Therefore:

P(A, H) = P(H) sum_b P(A | B=b)P(B=b | H)

By putting all together

P(A | H) = [P(H) sum_b P(A | B=b)P(B=b | H)] / P(H) = sum_b P(A | B=b)P(B=b | H)
"""
for acromantula in ['true', 'false']:
    for house in houses:
        cur_key = 'AcromantulaSighting={0}|House={1}'.format(acromantula, house)
        if cur_key not in pd:
            pd[cur_key] = 0
        for brave in ['true', 'false']:
            pd[cur_key] += pd['AcromantulaSighting={0}|Brave={1}'.format(acromantula, brave)]*pd['Brave={0}|House={1}'.format(brave,house)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

"""
9. Computing P(AcromantulaSighting | TimeOfDay, House):

We can use the Bayes' rule:

P(A | T, H) = P(T, H, A) / P(T, H)

We know that TimeOfDay is independent from House so

P(A | T, H) = P(T, H, A) / (P(T)P(H))

We can decompose the joint distribution P(T, H, A) with the chain rule

P(T, H, A) = P(T | H, A)P(H | A)P(A)

We know that TimeOfDay is independent from House, so we obtain

P(T, H, A) = P(T | A)P(H | A)P(A)

We need to compute P(T | A) and P(H | A). We can compute them
by using the Bayes' rule:

P(T | A) = P(A, T) / P(A)

P(H | A) = P(A | H)P(H) / P(A)
"""
for t in ['day', 'night']:
    for acromantula in ['true', 'false']:
        cur_key = 'TimeOfDay={0}|AcromantulaSighting={1}'.format(t, acromantula)
        pd[cur_key] = pd['AcromantulaSighting={0},TimeOfDay={1}'.format(acromantula, t)]/pd['AcromantulaSighting={0}'.format(acromantula)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

for house in houses:
    for acromantula in ['true', 'false']:
        cur_key = 'House={0}|AcromantulaSighting={1}'.format(house, acromantula)
        pd[cur_key] = (pd['AcromantulaSighting={0}|House={1}'.format(acromantula, house)]*pd['House={0}'.format(house)])/pd['AcromantulaSighting={0}'.format(acromantula)]
        print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

"""

And now we can finally compute P(A | T, H) by putting all together

P(A | T, H) = (P(T | A)P(H | A)P(A)) / (P(T)P(H))
"""
for acromantula in ['true', 'false']:
    for t in ['day', 'night']:
        for house in houses:
            cur_key = 'AcromantulaSighting={0}|TimeOfDay={1},House={2}'.format(acromantula, t, house)
            pd[cur_key] = (pd['TimeOfDay={0}|AcromantulaSighting={1}'.format(t, acromantula)]*pd['House={0}|AcromantulaSighting={1}'.format(house, acromantula)]*pd['AcromantulaSighting={0}'.format(acromantula)]) / \
            (pd['TimeOfDay={0}'.format(t)]*pd['House={0}'.format(house)])
            print('P({0}) = {1}'.format(cur_key, pd[cur_key]))

"""
10. Harry Potter is from Gryffindor. We already have P(A | T, H) so we don't need to compute it again

"""
print("\n>>>Answers:")

print("1. What are the chances of a Hogwarts' student to posses a pure heart?", pd['PureHeart=true'])
print("2. What is the likelihood of spotting a unicorn in the Forbidden Forest?", pd['UnicornSighting=true'])
print("3. What is the likelihood for a Hogwarts' student to be brave?", pd['Brave=true'])
print("4. What are the chances for a brave student to come from the Gryffindor house?", pd['House=gryffindor|Brave=true'])
print("5. What are the chances of seeing an acromantula in the Forbidden Forest?", pd['AcromantulaSighting=true'])
print("6. How likely is it to spot an acromantula at night?", pd['AcromantulaSighting=true|TimeOfDay=night'])
print("7. What are the chances for a brave student to encounter an acromantula?", pd['AcromantulaSighting=true|Brave=true'])
print("8. What is the likelihood of encountering an acromantula for a Ravenclaw student?", pd['AcromantulaSighting=true|House=ravenclaw'])
print("9. To what extent the likelihood of encountering an acromantula for a Ravenclaw student increases at night?", "It increases of " + str(pd['AcromantulaSighting=true|TimeOfDay=night,House=ravenclaw']/pd['AcromantulaSighting=true|House=ravenclaw']) + " times")
print("10. What is the likelihood for Harry Potter to encounter an acromantula when sneaking out from his dormroom during the night?", pd['AcromantulaSighting=true|TimeOfDay=night,House=gryffindor'])