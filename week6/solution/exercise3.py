from pgmpy.models import BayesianNetwork
from pgmpy.factors.discrete.CPD import TabularCPD

# Creating a Bayesian network and its nodes
hogwarts_bn = BayesianNetwork()
hogwarts_bn.add_nodes_from(
    ['UnicornSighting', 'PureHeart', 'AcromantulaSighting',
      'TimeOfDay', 'House', 'Brave']
)

# Adding dependencies
hogwarts_bn.add_edge('House', 'PureHeart')
hogwarts_bn.add_edge('House', 'Brave')
hogwarts_bn.add_edge('House', 'PureHeart')
hogwarts_bn.add_edge('PureHeart', 'UnicornSighting')
hogwarts_bn.add_edge('Brave', 'AcromantulaSighting')
hogwarts_bn.add_edge('TimeOfDay', 'AcromantulaSighting')

# Adding CPD tables to nodes
time_cpd = TabularCPD(
    variable='TimeOfDay',
    variable_card=2,
    values=[[0.5], [0.5]]
)
hogwarts_bn.add_cpds(time_cpd)

house_cpd = TabularCPD(
    variable='House',
    variable_card=4,
    values=[[0.25], [0.25], [0.25], [0.25]]
)
hogwarts_bn.add_cpds(house_cpd)

unicorn_cpd = TabularCPD(
    variable='UnicornSighting',
    variable_card=2,
    evidence=['PureHeart'],
    evidence_card=[2],
    values=[
        [0.1, 1/400], # UnicornSighting True
        [1 - 0.1, 1 - (1/400)] # UnicornSighting False
    ]
)
hogwarts_bn.add_cpds(unicorn_cpd)

heart_cpd = TabularCPD(
    variable='PureHeart',
    variable_card=2,
    evidence=['House'],
    evidence_card=[4],
    values=[
        [0.002, 0.002, 0.0001, 0.01], # PureHeart true
        [1 - 0.002, 1 - 0.002, 1 - 0.0001, 1 - 0.01] # PureHeart false
    ]
)
hogwarts_bn.add_cpds(heart_cpd)

brave_cpd = TabularCPD(
    variable='Brave',
    variable_card=2,
    evidence=['House'],
    evidence_card=[4],
    values=[
        [0.05, 0.02, 0.02, 0.01], # Brave true
        [1 - 0.05, 1 - 0.02, 1 - 0.02, 1 - 0.01] # Brave false
    ]
)
hogwarts_bn.add_cpds(brave_cpd)

acromantula_cpd = TabularCPD(
    variable='AcromantulaSighting',
    variable_card=2,
    evidence=['TimeOfDay', 'Brave'],
    evidence_card=[2, 2],
    values=[
        [0.01, 0.025, 0.05, 0.1], # AcromantulaSighting true
        [1 - 0.01, 1 - 0.025, 1 - 0.05, 1 - 0.1] # AcromantulaSighting false
    ]
)
hogwarts_bn.add_cpds(acromantula_cpd)

# checking that everything is ok
try:
    model_check = hogwarts_bn.check_model()
except Exception as e:
    model_check = e

if __name__ == "__main__":
    if model_check == True:
        print("The Bayesian network model does not have any error")
    else:
        print("Warning! There is an error in the Bayesian network model: {0}".format(e))